﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace fr_stringcalculator
{
    public class Program
    {
        const string DYNAMIC_DELIMITER_STARTSWITH = "//";
        static void Main(string[] args)
        {
            Console.Title = "Friendlyrentals string calculator kata";
            Console.WriteLine("Follow instructions on 'readme.md'");
            Console.ReadLine();
        }

        public static int Add(string numbers)
        {            
            if (!string.IsNullOrEmpty(numbers))
            {
                List<int> numbersList;
                if (numbers.StartsWith(DYNAMIC_DELIMITER_STARTSWITH))
                {
                    var indexFirstNewLine = numbers.IndexOf("\n", StringComparison.Ordinal);
                    var dynamicDelimiter = numbers.Substring(DYNAMIC_DELIMITER_STARTSWITH.Length, indexFirstNewLine - DYNAMIC_DELIMITER_STARTSWITH.Length).ToArray();
                    numbers = numbers.Substring(indexFirstNewLine, numbers.Length - indexFirstNewLine);
                    numbersList = numbers.Split(dynamicDelimiter).Select(int.Parse).ToList();
                }
                else 
                { 
                    var allowedDelimiters = new Char[] {',', '\n'};
                    numbersList = numbers.Split(allowedDelimiters).Select(int.Parse).ToList();
                }

                var negatives = numbersList.Where(number => number < 0).ToList();
                if (negatives.Any())
                {
                    throw new NegativesNotAllowed(string.Join(", ", negatives));
                }
                return numbersList.Sum();
            }
            return 0;
        }
    }
}
