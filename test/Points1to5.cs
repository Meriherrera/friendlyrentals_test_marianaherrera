﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using fr_stringcalculator;

namespace test
{
    [TestClass]
    public class Points1To5
    {
        [TestMethod]
        public void Allow0NumbersCommaDelimiter()
        {
            var input = string.Empty;
            var result = Program.Add(input);

            Assert.AreEqual(result, 0);
        }

        [TestMethod]
        public void Allow1NumberCommaDelimiter()
        {
            var input = "1";
            var result = Program.Add(input);

            Assert.AreEqual(result, 1);
        }

        [TestMethod]
        public void Allow2NumbersCommaDelimiter()
        {
            var input = "1,2";
            var result = Program.Add(input);

            Assert.AreEqual(result, 3);
        }

        [TestMethod]
        public void Allow5NumbersCommaDelimiter()
        {
            var input = "1,2,6,7,8";
            var result = Program.Add(input);

            Assert.AreEqual(result, 24);
        }

        [TestMethod]
        public void AllowNewLineDelimiter()
        {
            var input = "1\n2,3";
            var result = Program.Add(input);

            Assert.AreEqual(result, 6);
        }

        [TestMethod]
        public void AllowDynamicDelimiter()
        {
            var input = "//;\n1;2";
            var result = Program.Add(input);

            Assert.AreEqual(result, 3);
        }

        [TestMethod]
        public void NotAllow1Negative()
        {
            try
            {
                var input = "//;\n1;-2";
                Program.Add(input);
            }
            catch (NegativesNotAllowed n)
            {
                Assert.AreEqual(n.Message, string.Format(EnglishMessages.NegativesNotAllowed, "-2"));
            }
        }

        [TestMethod]
        public void NotAllowManyNegatives()
        {
            try
            {
                var input = "1,-2,-8";
                Program.Add(input);
            }
            catch (NegativesNotAllowed n)
            {
                Assert.AreEqual(n.Message, string.Format(EnglishMessages.NegativesNotAllowed, "-2, -8"));
            }
        }

        [TestMethod]
        [ExpectedException(typeof(NegativesNotAllowed))]
        public void NotAllowNegativesByDataAnnotation()
        {
            var input = "//;\n1;-2";
            Program.Add(input);
        }
    }
}
