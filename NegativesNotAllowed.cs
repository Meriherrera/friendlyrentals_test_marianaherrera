﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr_stringcalculator
{
    public class NegativesNotAllowed : Exception
    {
        public NegativesNotAllowed(string negatives)
            : base(string.Format(EnglishMessages.NegativesNotAllowed, negatives))
        {
        }
    }
}
